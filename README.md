# string-formatters

Useful Python functions for formatting common types of information as strings. E.g. timestamps, latitude-longitude, byte arrays, etc.