"""
    Convenient human-readable string formats for common situations like latitude-longitude output or timestamps
"""
import itertools
from datetime import datetime, timezone, timedelta


def str_lat_long(lat: float, long: float, prec=6):
    """ Human-readable latitude-longitude string representation

    E.g. lat = 32.468212, long = -94.722834, prec = 4 returns "32.4682°N, 94.7228°W"

    :param lat: the latitude, -90 to 90 deg, where positive is North of the equator
    :param long: the longitude, -180 to 180 deg, where positive is East of the equator
    :param prec: how many decimal places to include on the latitude and longitude
    :return: human-readable latitude-longitude string (see example above)
    """
    # Format latitude and longitude values based on specified precision
    number_formatter = '{{:.{}f}}'.format(prec)
    lat_str = number_formatter.format(abs(lat))
    long_str = number_formatter.format(abs(long))

    # Determine N, S, E, W additions based on sign of latitude/longitude
    NS = 'N' if lat >= 0 else 'S'
    EW = 'E' if long >= 0 else 'W'

    return '{}°{}, {}°{}'.format(lat_str, NS, long_str, EW)


def str_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp

    Not safe for inclusion in file names. See `str_file_safe_timestamp`

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d %H:%M:%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('%H:%M:%S')


def str_file_safe_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp safe to include in file names

    Contains no spaces or characters illegal in file names

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d_T%H-%M-%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('T%H-%M-%S')


def str_bytes(b: bytes, cap=True, group=2):
    """ Formats a bytes object as grouped hex digits, for easier reading

    e.g. b'\xca\xfe\xd0\x0d' -> CAFE D00D
    Note: if you want the string to start with 0x, append it yourself

    :param b: the bytes object to format
    :param cap: if True, hexadecimal letters are capitalized, lowercase otherwise (0x3f vs 0x3F)
    :param group: number of bytes to include in a group, e.g. 0->'CAFEDOOD', 1->'CA FE DO OD', or 2->'CAFE DOOD'
    :return: the formatted string of hex characters
    """
    str_hex = b.hex()
    if cap:
        str_hex = str_hex.upper()
    if group > 0:
        # Group characters and join into string
        n = group * 2
        hex_iters = [iter(str_hex)] * n
        hex_groups = itertools.zip_longest(*hex_iters, fillvalue='')
        str_hex = ' '.join(''.join(group) for group in hex_groups)
    return str_hex
